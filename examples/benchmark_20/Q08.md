
# input1

C001:Str,C002:Str,C003:Str,C004:Str,C005:Str,C006:Date,C007:Str,C008:Str,C009:Str,C010:Str,C011:Str,C012:Str,C013:Str,C014:Date,C015:Str,C016:Str,C017:Date,C018:Str,C019:Str,C020:Str
KEY_1,C002,C003,C004_1,C005_1,20200101,C007,C008_1,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020
KEY_2,C002,C003,C004_2,C005_2,20200101,C007,C008_2,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020
KEY_3,C002,C003,C004_3,C005_3,20200101,C007,C008_3,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020

# input2

C001:Str,C002:Str,C003:Str,C004:Str,C005:Str,C006:Date,C007:Str,C008:Str,C009:Str,C010:Str,C011:Str,C012:Str,C013:Str,C014:Date,C015:Str,C016:Str,C017:Date,C018:Str,C019:Str,C020:Str
C001,C002,C003,C004_1,KEY_2,20200101,C007,C008_1,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020
C001,C002,C003,C004_2,KEY_3,20200101,C007,C008_2,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020
C001,C002,C003,C004_3,KEY_4,20200101,C007,C008_3,C009,C010,C011,C012,C013,C014,C015,C016,C017,C018,C019,C020

# constraint

{
  "constants": [],
  "aggregation_functions": []
}

# output

C005:Str
C005_2
C005_3

# solution

```sql
-- join 2 tables: 10 cols x 10 cols

SELECT
    A0.C005
FROM
    INPUT1 AS A0
    JOIN INPUT2 AS B0
        ON A0.C005 = B0.C001
```
